#include "blockbadsitesmanage.h"
#include "ui_blockbadsitesmanage.h"
#include <QObject>
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>
#include <signal.h>

#define D 11000000



QString getMd5(const QString &password) {
    return QString(QCryptographicHash::hash(password.toUtf8(),
                                            QCryptographicHash::Md5).toHex());
}

blockBadSitesManage::blockBadSitesManage(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::blockBadSitesManage)
{
    QWidget::setWindowFlags(Qt::FramelessWindowHint);

    flag = false;
    checkPussword = true;

    int countP;
    loadSettings();
    Ti.start();

    if(!chackSafe()) {qApp->exit(); return;}

    if (settings.contains("Password") && !settings["Password"].isEmpty()) {
        for (countP = 0; countP < 3; countP ++){
            QDialog dlg;
            dlg.setWindowTitle("Enter password");
            QLineEdit *password = new QLineEdit;
            QPushButton *okButton = new QPushButton(tr("Ok")),
                    *cancelButton = new QPushButton(tr("Cancel"));
            connect(okButton, SIGNAL(clicked()), &dlg, SLOT(accept()));
            connect(cancelButton, SIGNAL(clicked()), &dlg, SLOT(reject()));
            password->setEchoMode(QLineEdit::Password);

            QHBoxLayout *layout1 = new QHBoxLayout;
            layout1->addWidget(new QLabel(tr("Password: ")));
            layout1->addWidget(password);

            QHBoxLayout *layout2 = new QHBoxLayout;
            layout2->addWidget(okButton);
            layout2->addWidget(cancelButton);

            QVBoxLayout *layout3 = new QVBoxLayout;
            layout3->addLayout(layout1);
            layout3->addLayout(layout2);
            dlg.setLayout(layout3);
            if (dlg.exec() != QDialog::Accepted) {qApp->exit(); return;}
            if (getMd5(password->text()) == settings["Password"]) {

                ui->setupUi(this);
                ui -> stackedWidget -> hide();
                QCoreApplication::setApplicationName("BlockBadSites");
                break;
            }
            else
            {
                QMessageBox::warning(0, QApplication::applicationName(),
                                     "The password is wrong.");

                if(countP == 2) checkPussword = false; // тут потрібно організувати вихід з програми
            }
        }
    }
        else {
            ui->setupUi(this);
            ui -> stackedWidget -> hide();
            QCoreApplication::setApplicationName("BlockBadSites");
        }




    ui -> stackedWidget -> hide();

}

blockBadSitesManage::~blockBadSitesManage()
{
    if(flag)
        saveSafe();
    delete ui;
}



bool blockBadSitesManage::loadSettings() {
    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QString s, section(""); int i;

    badWordsInAddress.clear();
    badWordsInContent.clear();
    badWords.clear();
    settings.clear();
    chackSf.clear();

    if (file.open(QFile::ReadOnly | QFile::Text) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
        return false;
    }


    while (file.atEnd() == false) {
        s = QString::fromUtf8(file.readLine()).trimmed();
        if (s.isEmpty()) continue;
        if (s.startsWith('[') && s.endsWith(']')) section = s.mid(1, s.size() - 2);
        else if (section == "Main") {
            if ((i = s.indexOf('=')) != -1) settings[s.left(i)] = s.mid(i + 1);
        }
        else if (section == "BadWords") badWords.append(s);
        else if (section == "BadWordsInAddress") badWordsInAddress.append(s);
        else if (section == "BadWordsInContent") badWordsInContent.append(s);
        else if (section == "System") chackSf.append(s);
    }
    file.close();

    return true;
}

void blockBadSitesManage::reloadSettings()
{
    loadSettings();
}

void blockBadSitesManage::saveSettings() {
    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QByteArray ba; int i1, i2;
    if (file.open(QFile::ReadOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
            "Open error of \"" + filePath + "\" file for read.");
        return;
    }
    ba = file.readAll();
    file.close();

    // Видалення секції [Main] та її параметрів з ba:
    if ( ((i1 = ba.indexOf("[Main]")) != -1) &&
         ((i2 = ba.indexOf("\n[", i1 + strlen("[Main]"))) != -1) )
        ba.remove(i1, i2 - i1 + 1);

    QString s; QByteArray lineBreak; QMapIterator<QString, QString> i(settings);
    lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
    s = "[Main]" + lineBreak;
    while (i.hasNext()) {
        i.next();
        s += (i.key() + "=" + i.value() + lineBreak);
    }

    if (file.open(QFile::WriteOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
            "Error of open the \"" + filePath + "\" file for write.");
        return;
    }
    file.write(s.toUtf8() + lineBreak + ba);
    file.close();
}



void blockBadSitesManage::savebadWordsInAddress()
{
    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QByteArray ba; int i1, i2;

    if (file.open(QFile::ReadOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
        return;
    }

    ba = file.readAll();
    file.close();

    // Видалення секції [Main] та її параметрів з ba:
    if ( ((i1 = ba.indexOf("[BadWordsInAddress]")) != -1) &&
                 ((i2 = ba.indexOf("\n[", i1 + strlen("[BadWordsInAddress]"))) != -1) )
        ba.remove(i1, i2 - i1 + 1);

    QString s; QByteArray lineBreak;
    lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
    s = "[BadWordsInAddress]" + lineBreak;

    s += badWordsInAddress.join(lineBreak); //тут потрібно замінити на правильне перечислення.
    //Можливо просто додати ітератор

    if (file.open(QFile::WriteOnly) == false) {
                QMessageBox::warning(0, QApplication::applicationName(),
                                     "Error of open the \"" + filePath + "\" file for write.");
                return;
    }
    file.write(s.toUtf8() + lineBreak + ba);
    file.close();
}



void blockBadSitesManage::savebadWordsInContent()
{
    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QByteArray ba; int i1, i2;

    if (file.open(QFile::ReadOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
        return;
    }

    ba = file.readAll();
    file.close();

    // Видалення секції [BadWordsInContent] та її параметрів з ba:
    if ( ((i1 = ba.indexOf("[BadWordsInContent]")) != -1) &&
                 ((i2 = ba.indexOf("\n[", i1 + strlen("[BadWordsInContent]"))) != -1) )
        ba.remove(i1, i2 - i1 + 1);

    QString s; QByteArray lineBreak;
    lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
    s = "[BadWordsInContent]" + lineBreak;

    s += badWordsInContent.join(lineBreak); //тут потрібно замінити на правильне перечислення.
    //Можливо просто додати ітератор

    if (file.open(QFile::WriteOnly) == false) {
                QMessageBox::warning(0, QApplication::applicationName(),
                                     "Error of open the \"" + filePath + "\" file for write.");
                return;
    }
    file.write(s.toUtf8() + lineBreak + ba);
    file.close();
}



void blockBadSitesManage::savebadWords()
{
    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QByteArray ba; int i1, i2;

    if (file.open(QFile::ReadOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
        return;
    }

    ba = file.readAll();
    file.close();

    // Видалення секції [BadWords] та її параметрів з ba:
    if ( ((i1 = ba.indexOf("[BadWords]")) != -1) &&
                 ((i2 = ba.indexOf("\n[", i1 + strlen("[BadWords]"))) != -1) )
        ba.remove(i1, i2 - i1 + 1);

    QString s; QByteArray lineBreak;
    lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
    s = "[BadWords]" + lineBreak;

    s += badWords.join(lineBreak); //тут потрібно замінити на правильне перечислення.
    //Можливо просто додати ітератор

    if (file.open(QFile::WriteOnly) == false) {
                QMessageBox::warning(0, QApplication::applicationName(),
                                     "Error of open the \"" + filePath + "\" file for write.");
                return;
    }
    file.write(s.toUtf8() + lineBreak + ba);
    file.close();
}



void blockBadSitesManage::on_changePasswordButton_clicked()
{
    ui -> changePasswordButton -> setStyleSheet("background:#78e7ff;");
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    ui->messageLabel->clear();

    if (!settings.contains("Password"))
    {
        ui -> lineEdit -> hide();
        ui -> label_2 -> hide();
        ui -> label_3 -> setText("Password:");
    }
    else
    {
        ui -> lineEdit -> show();
        ui -> label_2 -> show();
        ui -> label_3 -> setText("New password:");
    }

    ui -> stackedWidget->setCurrentIndex(3);
    moveButton();
    ui -> changePasswordButton -> setStyleSheet("background:#78e7ff;");
    ui -> stackedWidget -> show();
}

void blockBadSitesManage::on_blockedCategoriesButton_clicked()
{
    moveButton();
    ui -> stackedWidget->setCurrentIndex(0);
    if(ui -> plainTextEdit -> toPlainText() == "")
    ui -> plainTextEdit -> appendPlainText(badWords.join("\n")); // зробити перевірку чи не повний, якщо повний, не додавати
    if(ui -> plainTextEdit_2 -> toPlainText() == "")
    ui -> plainTextEdit_2 -> appendPlainText(badWordsInContent.join("\n"));

    ui -> blockedCategoriesButton -> setStyleSheet("background:#78e7ff;");

    ui -> stackedWidget -> show();

}

void blockBadSitesManage::on_exitButton_clicked()
{
    ui -> exitButton -> setStyleSheet("background:#78e7ff;");
    qApp->quit();
}

void blockBadSitesManage::on_HelpButton_clicked()
{

    ///if(ui->textBrowser->)



    QString filePath(QApplication::applicationDirPath() + "/help");
    QFile file(filePath); QString s;

    s.clear();




    if (file.open(QFile::ReadOnly | QFile::Text) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
    }
    else {

    while (file.atEnd() == false)
        s += QString::fromUtf8(file.readLine());

    file.close();
    }


    ui -> textBrowser -> setText(s);
    ui -> stackedWidget->setCurrentIndex(4);
    moveButton();
    ui -> HelpButton -> setStyleSheet("background:#78e7ff;");
    ui -> stackedWidget -> show();

}

void blockBadSitesManage::on_blockNewSiteButton_clicked()
{
     ui -> stackedWidget->setCurrentIndex(1);
     if(ui -> textEdit -> toPlainText() == "")
     ui -> textEdit-> append(badWordsInAddress.join("\n"));


     moveButton();

     ui -> blockNewSiteButton -> setStyleSheet("background:#78e7ff;");
     ui -> stackedWidget -> show();
}




void blockBadSitesManage::on_browsingHistoryButton_clicked()
{    
    ui -> stackedWidget->setCurrentIndex(2);
    moveButton();
    ui -> browsingHistoryButton -> setStyleSheet("background:#78e7ff;");
    ui -> stackedWidget -> show();

}

void blockBadSitesManage::on_blockedCategorisAcceptButton_clicked()
{
    QString str =  ui -> plainTextEdit -> toPlainText();
    badWords = str.split("\n");

    str.clear();
    str = ui -> plainTextEdit_2 -> toPlainText();
    badWordsInContent = str.split("\n");

    savebadWords();
    savebadWordsInContent();

    saverLocalSocket.reloadSetting();


    ui -> stackedWidget -> hide();
    moveButtonCenter();
}

void blockBadSitesManage::on_pushButton_clicked()
{
    ui->messageLabel->clear();

    if (settings.contains("Password") && !settings["Password"].isEmpty()){

    if (ui -> lineEdit -> text().isEmpty() || getMd5(ui -> lineEdit -> text()) != settings["Password"]) {
        ui->messageLabel->setText("The old password is wrong.");
    } else
        if (ui->lineEdit_2->text() != ui->lineEdit_3->text())
            ui->messageLabel->setText("The password confirmation is wrong.");
        else {
            if (ui->lineEdit_2->text().isEmpty()) settings.remove("Password");
            else settings["Password"] = getMd5(ui->lineEdit_2->text());
            saveSettings(); reloadSettings();
            ui->messageLabel->setText("The password changed!");
            ui->lineEdit->clear();
            ui->lineEdit_2->clear();
            ui->lineEdit_3->clear();
        }
    } else {

            if (ui->lineEdit_2->text() != ui->lineEdit_3->text())
                ui->messageLabel->setText("The password confirmation is wrong.");
            else {
                if (ui->lineEdit_2->text().isEmpty())
                {
                    if (settings.contains("Password")) settings.remove("Password");
                }
                else settings["Password"] = getMd5(ui->lineEdit_2->text());
                saveSettings();
                reloadSettings();

                saverLocalSocket.reloadSetting();

                ui->messageLabel->setText("The password changed!");
                ui->lineEdit->clear();
                ui->lineEdit_2->clear();
                ui->lineEdit_3->clear();
                }

    }


    if (settings.contains("Password"))
    {
        ui -> lineEdit -> show();
        ui -> label_2 -> show();
        ui -> label_3 -> setText("New password:");
    }
    else
    {
        ui -> lineEdit -> hide();
        ui -> label_2 -> hide();
        ui -> label_3 -> setText("Password:");
    }


}

//void blockBadSitesManage::on_pushButton_2_clicked()
//{
//    ui->lineEdit->clear();
//    ui->lineEdit_2->clear();
//    ui->lineEdit_3->clear();
//    ui->messageLabel->clear();
//    ui -> stackedWidget -> hide();
//}

void blockBadSitesManage::on_changeBedWordInAddressButton_clicked()
{
    QString str =  ui -> textEdit -> toPlainText();
    badWordsInAddress = str.split("\n");

    savebadWordsInAddress();

    saverLocalSocket.reloadSetting();

    ui -> stackedWidget -> hide();
    moveButtonCenter();
}


void blockBadSitesManage::moveButton()
{
    ui -> blockedCategoriesButton -> setGeometry(30,89,100,100);
    ui -> blockNewSiteButton -> setGeometry(30,194,100,100);
    ui -> changePasswordButton -> setGeometry(30,299,100,100);

    ui -> HelpButton -> setGeometry(134,89,100,100);
    ui -> browsingHistoryButton -> setGeometry(134,194,100,100);
    ui -> exitButton -> setGeometry(134,299,100,100);
    /// переробити щоб краще переключались кольори
    ///
    ui -> HelpButton -> setStyleSheet("background:#757575;");
    ui -> browsingHistoryButton -> setStyleSheet("background:#757575;");
    ui -> changePasswordButton -> setStyleSheet("background:#757575;");
    ui -> blockNewSiteButton -> setStyleSheet("background:#757575;");
    ui -> blockedCategoriesButton -> setStyleSheet("background:#757575;");
}

void blockBadSitesManage::moveButtonCenter()
{

    ui -> blockedCategoriesButton -> setGeometry(43,117,111,111);
    ui -> blockNewSiteButton -> setGeometry(190,117,111,111);
    ui -> browsingHistoryButton -> setGeometry(337,117,111,111);

    ui -> changePasswordButton -> setGeometry(43,237,111,111);
    ui -> HelpButton -> setGeometry(190,237,111,111);
    ui -> exitButton -> setGeometry(337,237,111,111);

}

void blockBadSitesManage::on_saveAddressListButton_clicked()
{
    ui -> stackedWidget -> hide();
    moveButtonCenter();
}


void blockBadSitesManage::saveSafe()
{

    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QByteArray ba; int i1, i2;

    if (file.open(QFile::ReadOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
        return;
        }

    ///отут має бути зчитування системно часу і збереження цих змінних

    tm += (int)Ti.elapsed();

    qDebug() << tm;


    chackSf.clear();

    chackSf.append(getMd5(QString::number(tm*13*184))); //змінна МД5
    chackSf.append(QString::number(tm*13)); //змінна


    ba = file.readAll();
    file.close();

    // Видалення секції та її параметрів з ba:
    if ( ((i1 = ba.indexOf("[System]")) != -1) &&
         ((i2 = ba.indexOf("\n[", i1 + strlen("[System]"))) != -1) )
        ba.remove(i1, i2 - i1 + 1);

    QString s; QByteArray lineBreak;
    lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
    s = "[System]" + lineBreak;

    s += chackSf.join(lineBreak); //тут потрібно замінити на правильне перечислення.
    //Можливо просто додати ітератор

    if (file.open(QFile::WriteOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Error of open the \"" + filePath + "\" file for write.");
        return;
    }
    file.write(s.toUtf8() + lineBreak + ba);
    file.close();
}


bool blockBadSitesManage::chackSafe()
{
    if(!chackSf.isEmpty() && !chackSf[0].isEmpty() && !chackSf[1].isEmpty())
    if (getMd5(QString::number(chackSf[1].toInt()*184)) == chackSf[0]) {
        if((tm = chackSf[1].toInt()/13) < D)
        {
            flag = true;
            return true;
        }
    }
    return false;
}
