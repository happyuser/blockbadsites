#include "saverlocalsocket.h"

SaverLocalSocket::SaverLocalSocket(QObject *parent) :
    QObject(parent)
{
    localSocket = new QLocalSocket();
}


SaverLocalSocket::~SaverLocalSocket()
{
    delete localSocket;
}


bool SaverLocalSocket::reloadSetting()
{
//   QByteArray data = "reload";

   localSocket -> connectToServer("LocalServer", QLocalSocket::WriteOnly);
//   localSocket -> write(data, data.length());
//   localSocket -> close();
   localSocket -> disconnectFromServer();

   return true;
}
