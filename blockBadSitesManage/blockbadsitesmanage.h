#ifndef BLOCKBADSITESMANAGE_H
#define BLOCKBADSITESMANAGE_H

#include <QMainWindow>
#include <QObject>
#include <QMap>
#include <QCryptographicHash>
#include <QFile>
#include <QTime>
#include <QtNetwork/QLocalSocket>
#include <saverlocalsocket.h>



namespace Ui {
class blockBadSitesManage;
}

class blockBadSitesManage : public QMainWindow
{
    Q_OBJECT

public:
    explicit blockBadSitesManage(QWidget *parent = 0);
    ~blockBadSitesManage();
     void showTrayIcon();
    QMap<QString, QString> settings;

    SaverLocalSocket saverLocalSocket;
    QStringList badWordsInAddress;
    QStringList badWordsInContent;
    QStringList badWords;
    int tm; // системний час
    QStringList chackSf;
    QTime Ti;
    bool checkPussword;
    bool chackSafe();

private:
    bool loadSettings();
    void saveSettings();
    void savebadWordsInAddress();
    void savebadWordsInContent();
    void savebadWords();

    void saveSafe();
    bool flag;


    void moveButton();
    void moveButtonCenter();


    Ui::blockBadSitesManage *ui;

private slots:
    void on_changePasswordButton_clicked();
    void on_exitButton_clicked();
    void on_HelpButton_clicked();
    void reloadSettings();
    void on_blockNewSiteButton_clicked();
    void on_blockedCategoriesButton_clicked();
    void on_browsingHistoryButton_clicked();
    void on_blockedCategorisAcceptButton_clicked();
    void on_pushButton_clicked();
//    void on_pushButton_2_clicked();
    void on_changeBedWordInAddressButton_clicked();
    void on_saveAddressListButton_clicked();
};

#endif // BLOCKBADSITESMANAGE_H
