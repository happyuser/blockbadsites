#ifndef SAVERLOCALSOCKET_H
#define SAVERLOCALSOCKET_H

#include <QObject>
#include <QLocalSocket>

class SaverLocalSocket : public QObject
{
    Q_OBJECT
public:
    explicit SaverLocalSocket(QObject *parent = 0);
    ~SaverLocalSocket();

    QLocalSocket *localSocket;

    bool reloadSetting();


signals:

public slots:

};

#endif // SAVERLOCALSOCKET_H
