QT += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = blockBadSitesManage
windows:CONFIG += static
TEMPLATE = app
LIBS += -lz
SOURCES += main.cpp blockbadsitesmanage.cpp saverlocalsocket.cpp
HEADERS += blockbadsitesmanage.h saverlocalsocket.h
FORMS += blockbadsitesmanage.ui
RESOURCES += res.qrc
