TEMPLATE = app
QT += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = BlockBadSites
linux:CONFIG += console
windows:CONFIG += static
LIBS += -lz
SOURCES += main.cpp blockbadsites.cpp proxyconnection.cpp
HEADERS += blockbadsites.h proxyconnection.h
RESOURCES = BlockBadSites.qrc
