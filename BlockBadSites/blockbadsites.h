#ifndef BLOCKBADSITES_H
#define BLOCKBADSITES_H
#include <QStringList>
#include <QTcpServer>
#include <QTime>
#define D 3600000


class BlockBadSites : public QObject {
    Q_OBJECT
public:
    explicit BlockBadSites(QObject *parent = 0);
    ~BlockBadSites();
	bool loadSettings();
	void showTrayIcon();
    bool chackSafe();
    void saveSafe();
    bool flag;
	QMap<QString, QString> settings;
	QStringList badWordsInAddress;
	QStringList badWordsInContent;
    int tm; // системний час
    QStringList chackSf;
    QTime Ti;

public slots:
	void on_newConnection();
	void reloadSettings();
	void changePassword();
	void aboutProgram();
	void quit();
private:
    // перші зміни
	void saveSettings();
    QTcpServer *proxyServer;
};
#endif
