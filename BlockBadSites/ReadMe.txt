						The release notes for BlockBadSites 1.1

The BlockBadSites program can block access to web pages if the bad word will be found in its address or content.
For a blocked HTTP pages a warning will be shown in the browser.
Note that for a HTTPS pages only the host name is checked becouse the content and full page address are encoded. BlockBadSites can not decode the data on HTTPS connection.

The BlockBadSites program is the proxy server that works on 6599 port. For the BlockBadSites work you may need to go to Control panel -> Internet options -> Connections and specify the address 127.0.0.1 and the port 6599 for proxy server.
The BlockBadSites can be run even in separate computer that is connected to the local network of your company. Just specify the IP address of that computer as proxy in the browser.

You can also try to run the Google Chrome browser with proxy by command:
path_to_google_chrome_exe_file --proxy-server="127.0.0.1:6599"

The bad words list may be specified in [BadWords], [BadWordsInAddress] and [BadWordsInContent] sections of settings.ini file.
The blocked hosts and full addresses of blocked HTTP pages may be specified in [BadWordsAddress] section.

The bad words that will be searched in both page address and content may be specified in the [BadWords] section.

The BlockBadSites program may be run in hidden mode in which its icon is not displayed near the clock. To use hidden mode specify
ShowTrayIcon=0
in the [Main] section of settings.ini file.
Then reload the BlockBadSites program if it is runing.

If you change the settings.ini file when BlockBadSites runs, click by right button on program icon near the clock and click "reload settings" item to apply the new settings.

BlockBadSites has a bug: in Internet Explorer and Midori browsers all HTTPS pages is not displayed. I hope to fix it later.

BlockBadSites was tested on Windows XP and Linux. If you have problem with it on new Windows version, let me to know.

