#include <QApplication>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QCryptographicHash>
#include <QFile>
#include "blockbadsites.h"
#include "proxyconnection.h"

QString getMd5(const QString &password) {
	return QString(QCryptographicHash::hash(password.toUtf8(),
											QCryptographicHash::Md5).toHex());
}
BlockBadSites::BlockBadSites(QObject *parent) :
QObject(parent), settings(), badWordsInAddress(), badWordsInContent() {
    flag = false;
    loadSettings(); //зробити захищене завантаження файлу
if(chackSafe()){
    Ti.start();
	QCoreApplication::setApplicationName("BlockBadSites");
	proxyServer = new QTcpServer(this);

	// whenever a user connects, it will emit signal.
	connect(proxyServer, SIGNAL(newConnection()), this, SLOT(on_newConnection()));

	if (!proxyServer->listen(QHostAddress::Any, 6599)) {
		QMessageBox::warning(0, QApplication::applicationName(),
			"Proxy server could not start.");
		qApp->quit();
		return;
	}

#ifdef Q_OS_LINUX
    printf("Proxy server started.\n");
#endif
}
}

BlockBadSites::~BlockBadSites(){
if(flag)
    saveSafe();
}


void BlockBadSites::on_newConnection() {
	QTcpSocket *socket = proxyServer->nextPendingConnection();
	if (socket->state() == QTcpSocket::ConnectedState) {
		printf("New connection established. Client address: %s.\n",
			socket->peerAddress().toString().toStdString().c_str());
		new ProxyConnection(socket);
	}
}

void BlockBadSites::showTrayIcon() {
	QAction *reloadSettings = new QAction("Reload settings", this);
	QAction *changePasswordAction = new QAction("Change password", this);
	QAction *aboutProgramAction = new QAction("About BlockBadSites", this);
	QAction *quitAction = new QAction("Quit", this);
	connect(reloadSettings, SIGNAL(triggered()), this, SLOT(reloadSettings()));
	connect(changePasswordAction, SIGNAL(triggered()), this, SLOT(changePassword()));
	connect(aboutProgramAction, SIGNAL(triggered()), this, SLOT(aboutProgram()));
	connect(quitAction, SIGNAL(triggered()), this, SLOT(quit()));

	QMenu *trayIconMenu = new QMenu;
	trayIconMenu->addAction(reloadSettings);
	trayIconMenu->addAction(changePasswordAction);
	trayIconMenu->addAction(aboutProgramAction);
	trayIconMenu->addAction(quitAction);

	QSystemTrayIcon *trayIcon = new QSystemTrayIcon(this);
	trayIcon->setContextMenu(trayIconMenu);
	trayIcon->setIcon(QIcon(":/icon.png"));
	trayIcon->setToolTip("BBS");
	trayIcon->show();
}

bool BlockBadSites::loadSettings() {
	QString filePath(QApplication::applicationDirPath() + "/settings.ini");
	QFile file(filePath); QString s, section(""); int i;
	badWordsInAddress.clear();
	badWordsInContent.clear();
	if (file.open(QFile::ReadOnly | QFile::Text) == false) {
		QMessageBox::warning(0, QApplication::applicationName(),
			"Open error of \"" + filePath + "\" file for read.");
		return false;
	}

	while (file.atEnd() == false) {
		s = QString::fromUtf8(file.readLine()).trimmed();
		if (s.isEmpty()) continue;
		if (s.startsWith('[') && s.endsWith(']')) section = s.mid(1, s.size() - 2);
		else if (section == "Main") {
			if ((i = s.indexOf('=')) != -1) settings[s.left(i)] = s.mid(i + 1);
		} else if (section == "BadWords") {
			badWordsInAddress.append(s);
			badWordsInContent.append(s);
		} else if (section == "BadWordsInAddress") badWordsInAddress.append(s);
		else if (section == "BadWordsInContent") badWordsInContent.append(s);
        else if (section == "System") chackSf.append(s);
	}
	file.close();
/*
	QMapIterator<QString, QString> iterator(settings);
	while (iterator.hasNext()) {
		iterator.next();
		qDebug() << iterator.key() << ":" << iterator.value();
	}
*/
	return true;
}

void BlockBadSites::reloadSettings() { loadSettings(); }

void BlockBadSites::saveSettings() {
	QString filePath(QApplication::applicationDirPath() + "/settings.ini");
	QFile file(filePath); QByteArray ba; int i1, i2;
	if (file.open(QFile::ReadOnly) == false) {
		QMessageBox::warning(0, QApplication::applicationName(),
			"Open error of \"" + filePath + "\" file for read.");
		return;
	}
	ba = file.readAll();
	file.close();

	// Видалення секції [Main] та її параметрів з ba:
	if ( ((i1 = ba.indexOf("[Main]")) != -1) &&
		 ((i2 = ba.indexOf("\n[", i1 + strlen("[Main]"))) != -1) )
		ba.remove(i1, i2 - i1 + 1);

	QString s; QByteArray lineBreak; QMapIterator<QString, QString> i(settings);
	lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
	s = "[Main]" + lineBreak;
	while (i.hasNext()) {
		i.next();
		s += (i.key() + "=" + i.value() + lineBreak);
	}

	if (file.open(QFile::WriteOnly) == false) {
		QMessageBox::warning(0, QApplication::applicationName(),
			"Error of open the \"" + filePath + "\" file for write.");
		return;
	}
	file.write(s.toUtf8() + lineBreak + ba);
	file.close();
}

void BlockBadSites::changePassword() {
	QDialog dlg; QGridLayout *layout = new QGridLayout; int i = 0;
	dlg.setWindowTitle("Change password");
	QLineEdit *oldPassword =
		((settings.contains("Password") && !settings["Password"].isEmpty()) ?
		new QLineEdit : NULL);
	QLineEdit *newPassword = new QLineEdit, *confirmation = new QLineEdit;
	QPushButton *okButton = new QPushButton(tr("Ok")),
				*cancelButton = new QPushButton(tr("Cancel"));
	connect(okButton, SIGNAL(clicked()), &dlg, SLOT(accept()));
	connect(cancelButton, SIGNAL(clicked()), &dlg, SLOT(reject()));
	newPassword->setEchoMode(QLineEdit::Password);
	confirmation->setEchoMode(QLineEdit::Password);

	if (oldPassword) {
		oldPassword->setEchoMode(QLineEdit::Password);
		layout->addWidget(new QLabel(tr("Old password: ")), i, 0);
		layout->addWidget(oldPassword, i, 1);
		i++;
	}

	layout->addWidget(new QLabel(tr("New password: ")), i, 0);
	layout->addWidget(newPassword, i, 1);
	i++;

	layout->addWidget(new QLabel(tr("Confirmation: ")), i, 0);
	layout->addWidget(confirmation, i, 1);
	i++;

	layout->addWidget(okButton, i, 0);
	layout->addWidget(cancelButton, i, 1);
	dlg.setLayout(layout);
	if (dlg.exec() != QDialog::Accepted) return;
	if (oldPassword && (getMd5(oldPassword->text()) != settings["Password"])) {
		QMessageBox::warning(0, QApplication::applicationName(),
							 "The old password is wrong.");
		return;
	}
	if (newPassword->text() != confirmation->text()) {
		QMessageBox::warning(0, QApplication::applicationName(),
							 "The password confirmation is wrong.");
		return;
	}
	if (newPassword->text().isEmpty()) {
		if (settings.contains("Password")) settings.remove("Password");
	} else settings["Password"] = getMd5(newPassword->text());
	saveSettings();
}

void BlockBadSites::aboutProgram() {
	QMessageBox::information(0, QApplication::applicationName(),
		"BlockBadSites version 1.1.\nSergiy Vovk. 2016.");
}

void BlockBadSites::quit() {
	if (settings.contains("Password") && !settings["Password"].isEmpty()) {
		QDialog dlg;
		dlg.setWindowTitle("Enter password");
		QLineEdit *password = new QLineEdit;
		QPushButton *okButton = new QPushButton(tr("Ok")),
					*cancelButton = new QPushButton(tr("Cancel"));
		connect(okButton, SIGNAL(clicked()), &dlg, SLOT(accept()));
		connect(cancelButton, SIGNAL(clicked()), &dlg, SLOT(reject()));
		password->setEchoMode(QLineEdit::Password);

		QHBoxLayout *layout1 = new QHBoxLayout;
		layout1->addWidget(new QLabel(tr("Password: ")));
		layout1->addWidget(password);

		QHBoxLayout *layout2 = new QHBoxLayout;
		layout2->addWidget(okButton);
		layout2->addWidget(cancelButton);

		QVBoxLayout *layout3 = new QVBoxLayout;
		layout3->addLayout(layout1);
		layout3->addLayout(layout2);
		dlg.setLayout(layout3);
		if (dlg.exec() != QDialog::Accepted) return;
		if (getMd5(password->text()) != settings["Password"]) {
			QMessageBox::warning(0, QApplication::applicationName(),
								 "The password is wrong.");
			return;
		}
	}
	qApp->quit();
}
void BlockBadSites::saveSafe()
{

    QString filePath(QApplication::applicationDirPath() + "/settings.ini");
    QFile file(filePath); QByteArray ba; int i1, i2;

    if (file.open(QFile::ReadOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Open error of \"" + filePath + "\" file for read.");
        return;
        }

    ///отут має бути зчитування системно часу і збереження цих змінних

    tm += (int)Ti.elapsed();

    qDebug() << tm;


    chackSf.clear();

    chackSf.append(getMd5(QString::number(tm*13*184))); //змінна МД5
    chackSf.append(QString::number(tm*13)); //змінна


    ba = file.readAll();
    file.close();

    // Видалення секції та її параметрів з ba:
    if ( ((i1 = ba.indexOf("[System]")) != -1) &&
         ((i2 = ba.indexOf("\n[", i1 + strlen("[System]"))) != -1) )
        ba.remove(i1, i2 - i1 + 1);

    QString s; QByteArray lineBreak;
    lineBreak = (ba.contains("\r\n") ? "\r\n" : "\n");
    s = "[System]" + lineBreak;

    s += chackSf.join(lineBreak); //тут потрібно замінити на правильне перечислення.
    //Можливо просто додати ітератор

    if (file.open(QFile::WriteOnly) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
                             "Error of open the \"" + filePath + "\" file for write.");
        return;
    }
    file.write(s.toUtf8() + lineBreak + ba);
    file.close();
}


bool BlockBadSites::chackSafe()
{
    if(!chackSf.isEmpty() && !chackSf[0].isEmpty() && !chackSf[1].isEmpty())
    if (getMd5(QString::number(chackSf[1].toInt()*184)) == chackSf[0]) {
        if((tm = chackSf[1].toInt()/13) < D)
        {
            flag = true;
            return true;
        }
    }
    return false;
}
