#ifndef PROXYCONNECTION_H
#define PROXYCONNECTION_H
#include <QObject>
#include <QTcpSocket>

class ProxyConnection : public QObject {
	Q_OBJECT
public:
	explicit ProxyConnection(QTcpSocket *clientSocket, QObject *parent = 0);
	void writeToLog(const QByteArray &ba);
public slots:
    void on_readyRead();  // отримання даних від браузеру
	void on_disconnected();
    void server_on_readyRead(); // отримання даних від серверу
	void server_on_disconnected();
private:
    // Ця функція обробляє перший запит CONNECT, GET, POST або HEAD від браузеру.
    void processFirstBrowserQuery(const QByteArray &query);
    // Функція checkContent перевіряє чи дозволений контент який отримали від серверу.
    bool checkContent(const QByteArray &content);
    // Функція checkChunkedData викликається після отримання останього фрагменту від серверу
    // якщо використовується технологія chunked.
	void checkChunkedData();
	QTcpSocket *socket; // сокет браузера
	QTcpSocket *serverSocket; // сокет сервера з яким зєднався браузер
	int contentLength;
	QByteArray receivedDataFromServer;
	bool isChunked;
	bool isContentGzip;
	bool wasFirstDataPacketGetFromServer;
	bool isContentTextHtml;
	bool isHttps;
};
#endif // PROXYCONNECTION_H
