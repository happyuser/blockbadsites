#include <QApplication>
#include <QFile>
#include "blockbadsites.h"

// Вказівка на екземпляр головного класу програми:
BlockBadSites *pBlockBadSites = NULL;

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
//	QString logFilePath(QApplication::applicationDirPath() + "/log.txt");
//	if (QFile::exists(logFilePath)) QFile::remove(logFilePath);
	a.setQuitOnLastWindowClosed(false);
	BlockBadSites bbs;
    if(!bbs.chackSafe()) {qDebug() << "exit"; return -1;}
	pBlockBadSites = &bbs;
	if (!bbs.loadSettings()) return 1;

	if (bbs.settings.contains("ShowTrayIcon") && bbs.settings["ShowTrayIcon"] == "1")
		bbs.showTrayIcon();
	return a.exec();
}
