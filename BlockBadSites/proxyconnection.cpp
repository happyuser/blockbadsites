#include <QApplication>
#include <QFile>
#include <QDebug>
#include <zlib.h>
#include "blockbadsites.h"
#include "proxyconnection.h"
#ifdef Q_OS_LINUX
#define show_tranfered_data
#endif
extern BlockBadSites *pBlockBadSites;

#define GZIP_WINDOWS_BIT 15 + 16
#define GZIP_CHUNK_SIZE 32 * 1024

/**
 * @brief Decompresses the given buffer using the standard GZIP algorithm
 * @param input The buffer to be decompressed
 * @param output The result of the decompression
 * @return @c true if the decompression was successfull, @c false otherwise
 */
bool gzipDecompress(QByteArray input, QByteArray &output) {
	// Prepare output
	output.clear();

    // Is there somethin+g to do?
	if (input.length() > 0) {
		// Prepare inflater status
		z_stream strm;
		strm.zalloc = Z_NULL;
		strm.zfree = Z_NULL;
		strm.opaque = Z_NULL;
		strm.avail_in = 0;
		strm.next_in = Z_NULL;

		// Initialize inflater
		int ret = inflateInit2(&strm, GZIP_WINDOWS_BIT);

		if (ret != Z_OK) return false;

		// Extract pointer to input data
		char *input_data = input.data();
		int input_data_left = input.length();

		// Decompress data until available
		do {
			// Determine current chunk size
			int chunk_size = qMin(GZIP_CHUNK_SIZE, input_data_left);

			// Check for termination
			if (chunk_size <= 0) break;

			// Set inflater references
			strm.next_in = (unsigned char*)input_data;
			strm.avail_in = chunk_size;

			// Update interval variables
			input_data += chunk_size;
			input_data_left -= chunk_size;

			// Inflate chunk and cumulate output
			do {
				// Declare vars
				char out[GZIP_CHUNK_SIZE];

				// Set inflater references
				strm.next_out = (unsigned char*)out;
				strm.avail_out = GZIP_CHUNK_SIZE;

				// Try to inflate chunk
				ret = inflate(&strm, Z_NO_FLUSH);

				switch (ret) {
				case Z_NEED_DICT:
					ret = Z_DATA_ERROR;
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
				case Z_STREAM_ERROR:
					// Clean-up
					inflateEnd(&strm);

					// Return
					return false;
				}

				// Determine decompressed size
				int have = (GZIP_CHUNK_SIZE - strm.avail_out);

				// Cumulate result
				if (have > 0) output.append((char*)out, have);

			} while (strm.avail_out == 0);
		} while (ret != Z_STREAM_END);

		// Clean-up
		inflateEnd(&strm);

		// Return
		return (ret == Z_STREAM_END);
	} else return true;
}

ProxyConnection::ProxyConnection(QTcpSocket *clientSocket, QObject *parent)
: QObject(parent), receivedDataFromServer("")
{
	contentLength = 0;
	isChunked = false;
	isContentGzip = false;
	isContentTextHtml = false;
	isHttps = false;
	wasFirstDataPacketGetFromServer = false;
	serverSocket = NULL;
	socket = clientSocket;
	connect(socket, SIGNAL(readyRead()), this, SLOT(on_readyRead()));
	connect(socket, SIGNAL(disconnected()), this, SLOT(on_disconnected()));
}

void ProxyConnection::writeToLog(const QByteArray &ba)
{
	QFile f(QApplication::applicationDirPath() + "/log.txt");
	if (f.open(QIODevice::Append)) { f.write(ba + "\r\n"); f.close(); }
}

// Ця функція обробляє перший запит CONNECT, GET, POST або HEAD від браузеру.
void ProxyConnection::processFirstBrowserQuery(const QByteArray &query)
{
	QByteArray s; QList<QByteArray> list; int i;
	i = query.indexOf("\r\n");
	if (i == -1) {
		if (socket->isOpen()) socket->close();
		return;
	}
	s = query.left(i);
	list = s.split(' ');

	if (list.count() > 1) {
		bool b = false;
		for (i = 0; i < pBlockBadSites->badWordsInAddress.count(); i++) {
			if (list[1].contains(pBlockBadSites->badWordsInAddress[i].toUtf8()) ||
list[1].toLower().contains(pBlockBadSites->badWordsInAddress[i].toUtf8())
				) { b = true; break; }
		}
		if (b) {
			if (query.startsWith("GET ")) {
				QByteArray message(
"<html><head><title>Notice</title></head><body>\r\n\
<h1 style='color: red; text-align: center'>This web page is blocked.</h1>\r\n\
</body></html>");
				socket->write(
"HTTP/1.1 404 Not Found\r\n\
Content-Length: " + QByteArray::number(message.size()) + "\r\n\
Content-Type: text/html; charset=iso-8859-1\r\n\r\n" + message);
			}
			if (socket->isOpen()) socket->close();
			return;
		}
	}

	int i1 = query.indexOf("\r\nHost: "), i2;
	if (i1 == -1) { if (socket->isOpen()) socket->close(); return; }
	i1 += 8;
	i2 = query.indexOf("\r\n", i1);
	if (i2 == -1) { if (socket->isOpen()) socket->close(); return; }
	QList<QByteArray> hostParameters;
	s = query.mid(i1, i2 - i1);
//	printf("\"%s\"\n", s.constData());
	hostParameters = s.split(':');
	isHttps = ((hostParameters.count() == 2) && (hostParameters[1] == "443"));

	serverSocket = new QTcpSocket(this);
	connect(serverSocket, SIGNAL(readyRead()), this, SLOT(server_on_readyRead()));
	connect(serverSocket, SIGNAL(disconnected()), this, SLOT(server_on_disconnected()));

	serverSocket->connectToHost(hostParameters[0],
		(hostParameters.count() == 2) ? hostParameters[1].toInt() : 80);

	if (serverSocket->waitForConnected(5000)) {
		printf("Connected to server %s\n", s.constData());
		if (query.startsWith("CONNECT "))
			socket->write("HTTP/1.0 200 Connection established\r\n\r\n");
		else serverSocket->write(query);
	} else { // якщо помилка підключення до серверу
		printf("Server %s is not connected.\n", s.constData());
		disconnect(serverSocket, SIGNAL(disconnected()));
		disconnect(serverSocket, SIGNAL(readyRead()));
		delete serverSocket;
		serverSocket = NULL;
	}
}

void ProxyConnection::on_readyRead() { // отримання даних від браузеру
	QByteArray ba(socket->readAll());
    if (isHttps) qDebug() << "-----------it is https-------------\n >" ;
        qDebug() << "-------------------Browser---------------------------\n >" << ba;
	if (serverSocket == NULL) { // якщо отримали першу порцію даних від браузеру
		if (ba.startsWith("CONNECT ") || ba.startsWith("GET ") ||
				ba.startsWith("POST ") || ba.startsWith("HEAD "))
			processFirstBrowserQuery(ba);
		else {
			printf("Unknown query from the browser.\n");
			if (socket->isOpen()) socket->close();
#ifdef Q_OS_LINUX
			qApp->quit();
#endif
		}
		return;
	} // if (serverSocket == NULL)
	serverSocket->write(ba);
}

void ProxyConnection::server_on_readyRead()  // отримання даних від серверу
{
	QByteArray ba(serverSocket->readAll());
	if (isHttps) { socket->write(ba); return; } // https не перевіряється.

	// Якщо отримали не перший пакунок даних від серверу:
	if (wasFirstDataPacketGetFromServer) {
		if (isContentTextHtml) {

			if (isChunked) { // якщо контент передано по технології chunked
				receivedDataFromServer += ba;
				// Якщо отримано останій фрагмент:
				if (receivedDataFromServer.endsWith("\r\n0\r\n") ||
					receivedDataFromServer.endsWith("\r\n0\r\n\r\n")) checkChunkedData();
				return;
			}

			if (contentLength) { // якщо відома довжина контенту
				receivedDataFromServer += ba;
				int i = receivedDataFromServer.indexOf("\r\n\r\n");
				if (i == -1) return;
				QByteArray content(receivedDataFromServer.mid(i + 4));
// Якщо сontent.size() < сontentLength то сервер передасть наступні шматки пізніше.
				if (content.size() < contentLength) return;
				if (isContentGzip) {
					QByteArray s;
					if (gzipDecompress(content, s)) content = s;
					else { content = ""; printf("Decompress error.\n"); }
				}
				if (checkContent(content)) socket->write(receivedDataFromServer);
				return;
			} // if (contentLength)

		} // if (isContentTextHtml)
	} else { // якщо отримали перший пакунок даних від серверу
		wasFirstDataPacketGetFromServer = true;
//		qDebug() << "Server >" << ba;
//		writeToLog(ba);

		if (ba.contains("\r\nContent-Type: text/html")) {
			isContentTextHtml = true;
			isContentGzip = ba.contains("\r\nContent-Encoding: gzip\r\n");
			int i, i2;
			if ((i = ba.indexOf("\r\n\r\n")) != -1) {
				QByteArray content(ba.mid(i + 4));

				if ((i = ba.indexOf("\r\nContent-Length: ")) != -1) {
					i += strlen("\r\nContent-Length: ");
					if ((i2 = ba.indexOf("\r\n", i)) != -1) {
						contentLength = ba.mid(i, i2 - i).toInt();
// Якщо сontent.size() < сontentLength то сервер передасть наступні шматки пізніше.
						if (content.size() < contentLength)
							{ receivedDataFromServer = ba; return; }
					}
				}

				// chunked - фрагментований.
				// https://ru.wikipedia.org/wiki/Chunked_transfer_encoding
				// Якщо контент передано по технології chunked:
				if (ba.contains("\r\nTransfer-Encoding: chunked\r\n")) {
					isChunked = true;
					receivedDataFromServer = ba;
					// Якщо отримано останій фрагмент:
					if (ba.endsWith("\r\n0\r\n") || ba.endsWith("\r\n0\r\n\r\n"))
						checkChunkedData();
					return;
				}

				if (isContentGzip) {
					QByteArray s;
					if (gzipDecompress(content, s)) content = s;
					else { content = ""; printf("Decompress error.\n"); }
				}
				if (!checkContent(content)) return;
			}
		}
	}
	socket->write(ba);
}

// Функція checkContent перевіряє чи дозволений контент який отримали від серверу.
bool ProxyConnection::checkContent(const QByteArray &content)
{
	int index, i; bool b = true; QByteArray s;
	for (index = 0; index < pBlockBadSites->badWordsInContent.count(); index++) {
		s = pBlockBadSites->badWordsInContent[index].toUtf8();
		i = content.indexOf(s);
		if (i == -1) {
			i = content.toLower().indexOf(s);
			if (i == -1) continue;
		}
		if (i == 0) { b = false; break; } // якщо s знайдено в content на початку
		// Якщо s знайдено в content не на початку:
		if (strchr("<>[](),.?!'\"/\\:;+- ", content.at(i - 1))) { b = false; break; }
	}

	if (!b) {
		QByteArray message(
"<html><head><title>Notice</title></head><body>\r\n\
<h1 style='color: red; text-align: center'>This web page is blocked.</h1>\r\n\
</body></html>");
		socket->write(
"HTTP/1.1 404 Not Found\r\n\
Content-Length: " + QByteArray::number(message.size()) + "\r\n\
Content-Type: text/html; charset=iso-8859-1\r\n\r\n" + message);
		if (socket->isOpen()) socket->close();
		if (serverSocket->isOpen()) serverSocket->close();
	}
	return b;
}

// Функція checkChunkedData викликається після отримання останього фрагменту від серверу
// якщо використовується технологія chunked.
void ProxyConnection::checkChunkedData()
{
	int i = receivedDataFromServer.indexOf("\r\n\r\n"), chunkSize;
	if (i == -1) return;
	QByteArray s(receivedDataFromServer.mid(i + 4)), content(""); bool ok;

	while (!s.startsWith("0\r\n")) {
		if (s.isEmpty()) break;
		i = s.indexOf("\r\n");
		if (i == -1) return;
		chunkSize = s.left(i).toInt(&ok, 16);
		if (!ok) return;
		content += s.mid(i + 2, chunkSize);
		s.remove(0, i + chunkSize + 2);
		if (s.startsWith("\r\n")) s.remove(0, 2);
    }

	if (isContentGzip) {
		s = "";
		if (gzipDecompress(content, s)) content = s;
		else { content = ""; printf("Decompress error.\n"); }
	}
	if (checkContent(content)) socket->write(receivedDataFromServer);
}

void ProxyConnection::on_disconnected()
{
	printf("Connection disconnected.\n");
	disconnect(socket, SIGNAL(disconnected()));
	disconnect(socket, SIGNAL(readyRead()));
	socket->deleteLater();
	deleteLater();
}

void ProxyConnection::server_on_disconnected()
{
	printf("Server disconnected.\n");
	if (socket->isOpen()) socket->close();
	if (serverSocket != NULL) {
		disconnect(serverSocket, SIGNAL(disconnected()));
		disconnect(serverSocket, SIGNAL(readyRead()));
        serverSocket->deleteLater();
		serverSocket = NULL;
	}
}
