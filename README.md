The BlockBadSites program can block access to web pages if the bad word will be found in its address or content.
For a blocked HTTP pages a warning will be shown in the browser.

The BlockBadSites was developed by C++ language.

The used technologies are:

* Qt 5 library.

* Socket.

* Qt creator as IDE for developing the application.

Demo video: http://vps1.happyuser.info/blockbadsites_demo.ogv
